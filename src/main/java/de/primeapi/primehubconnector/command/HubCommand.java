package de.primeapi.primehubconnector.command;

import de.primeapi.primehubconnector.config.Conf;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

import java.util.Random;

public class HubCommand extends Command {
    public HubCommand(String name) {
        super(name);
    }

    public void execute(CommandSender commandSender, String[] strings) {
        if(commandSender instanceof ProxiedPlayer){
            ProxiedPlayer p = (ProxiedPlayer) commandSender;
            String servername;
            if(Conf.cfg.getInt("lobby.amount") == 1){
                servername = Conf.cfg.getString("lobby.name");
            }else {
                int number = new Random().nextInt(Conf.cfg.getInt("lobby.amount")) + 1;
                servername = Conf.cfg.getString("lobby.name") + number;
            }

            if(p.getServer().getInfo().getName().equalsIgnoreCase(servername) || p.getServer().getInfo().getName().startsWith(servername)){
                p.sendMessage(ChatColor.translateAlternateColorCodes('&', Conf.cfg.getString("msg.alreadyOnHub")));
                return;
            }
            try {
                p.connect(ProxyServer.getInstance().getServerInfo(servername));
                p.sendMessage(ChatColor.translateAlternateColorCodes('&', Conf.cfg.getString("msg.connecting")).replaceAll("%server%", servername));
            }catch (Exception ex){
                p.sendMessage(ChatColor.translateAlternateColorCodes('&', Conf.cfg.getString("msg.falure")).replaceAll("%server%", servername));
            }

        }
    }
}
