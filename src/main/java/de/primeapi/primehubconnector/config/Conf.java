package de.primeapi.primehubconnector.config;

import net.md_5.bungee.config.Configuration;
import net.md_5.bungee.config.ConfigurationProvider;
import net.md_5.bungee.config.YamlConfiguration;

import java.io.File;
import java.io.IOException;

public class Conf {

    public static File file;
    public static Configuration cfg;

    public static void init(){
        File ord = new File("plugins/PrimeHubConnector");
        if(!ord.exists()){
            ord.mkdir();
        }

        file = new File("plugins/PrimeHubConnector", "config.yml");

        if(!file.exists()){
            try {
                file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        try {
            cfg = ConfigurationProvider.getProvider(YamlConfiguration.class).load(file);
            if(!cfg.contains("lobby.amount")){
                cfg.set("lobby.amount", 1);
                cfg.set("lobby.name", "Lobby-1");
                cfg.set("msg.connecting", "§7Connecting to %server%");
                cfg.set("msg.falure", "§cAn error appeared while connecting to %server%");
                cfg.set("msg.alreadyOnHub", "§cYou're already on Hub!");

                save();

            }
        } catch (IOException e) {
            e.printStackTrace();
        }



    }


    public static void save(){
        try {
            ConfigurationProvider.getProvider(YamlConfiguration.class).save(cfg, file);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
