package de.primeapi.primehubconnector;

import de.primeapi.primehubconnector.command.HubCommand;
import de.primeapi.primehubconnector.config.Conf;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.plugin.Plugin;

public class PrimeHubConnector extends Plugin {

    @Override
    public void onEnable() {
        super.onEnable();

        Conf.init();
        ProxyServer.getInstance().getPluginManager().registerCommand(this, new HubCommand("hub"));
        ProxyServer.getInstance().getPluginManager().registerCommand(this, new HubCommand("l"));
        ProxyServer.getInstance().getPluginManager().registerCommand(this, new HubCommand("lobby"));

    }

    @Override
    public void onDisable() {
        super.onDisable();
    }

}
